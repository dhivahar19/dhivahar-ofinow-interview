# Vue Registration App with Nodejs API, MongoDb and Vuex state managment

## Project setup
```
npm install
```
### Run the nodejs server for mongodb connection and REST API
```
node server.js
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

![Screen Shot 2019-01-19 at 00.44.45.png](https://bitbucket.org/repo/Bg4oMyb/images/2892506187-Screen%20Shot%202019-01-19%20at%2000.44.45.png)
![Screen Shot 2019-01-19 at 00.49.13.png](https://bitbucket.org/repo/Bg4oMyb/images/264745451-Screen%20Shot%202019-01-19%20at%2000.49.13.png)
![Screen Shot 2019-01-19 at 00.49.53.png](https://bitbucket.org/repo/Bg4oMyb/images/1612313961-Screen%20Shot%202019-01-19%20at%2000.49.53.png)
![Screen Shot 2019-01-19 at 00.50.06.png](https://bitbucket.org/repo/Bg4oMyb/images/743787583-Screen%20Shot%202019-01-19%20at%2000.50.06.png)
