var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Users
var User = new Schema({
  name: {
    type: String
  },
  email: {
    type: String
  },
  password: {
    type: String
  }
},{
    collection: 'users'
});

module.exports = mongoose.model('User', User);
