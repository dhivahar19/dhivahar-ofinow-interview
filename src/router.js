import Vue from 'vue';
import Router from 'vue-router';

import CreateUser from '@/components/CreateUser.vue';
import DisplayUser from '@/components/DisplayUser.vue';
import EditUser from '@/components/EditUser.vue';

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      name: 'CreateUser',
      path: '/create/user',
      component: CreateUser
    },
    {
        name: 'DisplayUser',
        path: '/',
        component: DisplayUser
    },
    {
        name: 'EditUser',
        path: '/edit/:id',
        component: EditUser
    }
  ]
})
