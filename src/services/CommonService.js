import Api from '@/services/Api'

export default {
    fetchItems () {
        return Api().get('users');
    },
    deleteItem (params){
        return Api().get('users/delete/' + params);
    },
    addItem (params){
        return Api().post('users/add', params);
    },
    getItem (params){
        return Api().get('users/edit/' + params);
    },
    updateItem (params){
        return Api().post('users/update/' + params._id, params);
    }

}