import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import CommonService from '@/services/CommonService'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
  state: {
    items : [],
    selectedItem : null
  },
  mutations: {
    SET_ITEMS (state, items) {
      state.items = items
    },
    REMOVE_ITEM (state, item){
      state.items = state.items.filter(obj => obj._id !== item._id)
    },
    ADD_ITEM (state, item){
      state.items = [...state.items, item]
    },
    GET_ITEM (state, item) {
      state.selectedItem = state.items.find(obj => obj._id === item)
    },
    UPDATE_ITEM (state, item){
      const itemIndex = state.items.findIndex(obj => obj._id === item._id);
      state.items.splice(itemIndex, 1, item)
    }
  },
  actions: {
    loadItems ({ commit }) {
      CommonService.fetchItems().then((response)=>{
        commit('SET_ITEMS', response.data)
      });      
    },
    removeItem ({ commit }, payload) {
        CommonService.deleteItem(payload._id).then(()=>{
          commit('REMOVE_ITEM', payload)
        });      
    },
    addItem ({ commit }, payload) {
        CommonService.addItem(payload).then(()=>{
          commit('ADD_ITEM', payload)
        });      
    },
    getItem ({ commit }, payload) {
        commit('GET_ITEM', payload)
    },
    updateItem ({ commit }, payload) {
      CommonService.updateItem(payload).then(()=>{
        commit('UPDATE_ITEM', payload)
      });
    }
  }
})
