import { expect } from 'chai'
import { mount } from '@vue/test-utils';
import CreateUser from '@/components/CreateUser.vue'
const wrapper = mount(CreateUser);

describe('CreateUser', () => {
    it('page title should be register', () => {
        //expect(wrapper.html()).contain('<h1>Register</h1>')
        expect(wrapper.find('h1').text()).equal("Register")
    });

    it('it should display the name when I write it', () => {
        expect(wrapper.vm.$data.item.name).to.equal(null);

        const input = wrapper.find('#name');
        input.element.value = 'Stefan';
        input.trigger('input');
        expect(wrapper.vm.$data.item.name).to.equal('Stefan');
    });

    it('it should display the email when I write it', () => {
        expect(wrapper.vm.$data.item.email).to.equal(null);
    
        const input = wrapper.find('#email');
        input.element.value = 'stefan@gmail.com';
        input.trigger('input');
        expect(wrapper.vm.$data.item.email).to.equal('stefan@gmail.com');
    });

    it('it should display the password', () => {
        expect(wrapper.vm.$data.item.password).to.equal(null);
    
        const input = wrapper.find('#password');
        input.element.value = 'password';
        input.trigger('input');
        expect(wrapper.vm.$data.item.password).to.equal('password');
    });
    it('password should be greater than 8 chars', () => {
    
        const input = wrapper.find('input#password');
        input.element.value = '1234567';
        input.trigger('input');
        wrapper.find('button').trigger('click');
        expect(wrapper.vm.$data.errors.length).to.equal(0)
    });
});